# SGO license server container #

This is an unofficial hack to utilize SGOs license server in linux
docker containers for floating license support in virtual machines.
It's based on similar FlexNet related solutions for other applications 
(e.g. [ADLMFlexNetServer](https://github.com/haysclark/docker-adlmflexnetserver)).

You may use this scripts to build the needed containers from scatch,
but you can also use the ready made ones provided via GitLab container
registry.

## Usage

### License Activation

It's very important to always use the same unique MAC address an all
license server related container invocations. You will get a plausible
MAC address for your container by reusing a slightly incremented
variante of the MAC address of the physical network card in your
machine (as reported by `ip link show eth0` or the like).

To activate SGO software products, you first have to know the FlexNet
SystemID of your license mangers host. This can be archived in
different ways. The most simple one is perhaps utilizing the `getlic`
command line tool:

    docker run -ti --rm --mac-address="xx:xx:xx:xx:xx:xx" \
		registry.gitlab.com/mash-graz/sgo-license-container getlic
	
You will get a long system identification string, which an be used in
SGOs Activation Wizard at SGOs shop page under
["Activation Codes"](https://www.sgo.es/my-account/activation-codes/).

### Starting the License Server

A container running the SGO License Server can be started by:

	docker run -d -p 1116:1116 --restart unless-stopped \
		 --mac-address="xx:xx:xx:xx:xx:xx" \
		 -v ABSOLUTE_PATH_TO_LICENSE_DIR:/var/flexlm \
		 --name sgo-license-server \ 
		 registry.gitlab.com/mash-graz/sgo-license-container

To archive a persistence, you have to specify a directory
(`ABSOLUTE_PATH_TO_LICENSE_DIR`) to hold the actual license files managed by the
license server.

The licensing server container exposes port 1116 on the docker host
for communication with its clients.

The license server is running in verbose mode. You can watch it's
output by utilizing dockers logging related commands:

	docker logs sgo-license-server
	
### Using Floating Lincenses on the client side

If you want to use the running license server on a remote host, you
have to use the *settings wheel* on the right side of mistikas licence
related dialog menus and enter the IP address of the licensing server.
You will have to enter your e-mail address and activation code (you
will find it in the mail from SGO confirming your order) and mistika
insight should be finally ready to run. 


	
	
