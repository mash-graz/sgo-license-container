FROM debian:testing

RUN apt-get update \
  && apt-get install -y libglib2.0-0 \
  && apt-get install -y --no-install-recommends libgl1-mesa-glx \
  && apt-get install -y libgl1-mesa-dri mesa-utils \
  && apt-get install -y --no-install-recommends libqt5gui5 \
  && apt-get install -y libqt5widgets5 libqt5xml5 libqt5xmlpatterns5 \
  && apt-get install -y ca-certificates \
  && rm -rf /var/lib/apt/lists/*

COPY getlic SGOActivationTool.bin sgoLicenseServer.bin /usr/local/bin/

EXPOSE 1116

CMD /usr/local/bin/sgoLicenseServer.bin -v