#!/bin/sh

# this script will downlad a mistika installer package for linux
# end extract the license server related binaries
#
# you will find actual linux mistika installers here:
# https://www.sgo.es/files/

URL="https://www.sgo.es/forums.vb/SGOI+D/SGOMistika.8.6.6-20170617.RC.Opt.Linux64.run.tar"

if [ ! -f `basename ${URL}` ] ; then
    wget "$URL"
else
    echo "installer already present... (don't download)"
fi

tar xf `basename ${URL}` `basename ${URL} .tar`
./`basename ${URL} .tar` --target mistika_installer --noexec
rm ./`basename ${URL} .tar`
mistika_installer/`basename ${URL} .run.tar` --dump-binary-data -o binary_data
rm -r mistika_installer

7z -aoa x binary_data/es.sgo.SGOMistika/*bin.7z \
   bin/sgoLicenseServer.bin \
   bin/SGOActivationTool.bin \
   bin/getlic

rm -r binary_data
